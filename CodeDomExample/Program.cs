﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using Microsoft.CSharp;

namespace CodeDomExample {
    class MainClass {
        public static void Main(string[] args) {
            Console.WriteLine("Enter C# code to be evaluated and returned.");
            Console.WriteLine("Press Enter to compile.");
            Console.WriteLine("Enter 'quit' or 'exit' to close the program.");
            Console.WriteLine("©Copyright 2017 by Dominik \"Zatherz\" Banaszak.");
            Console.WriteLine("Licensed under the terms of the MIT license, available as LICENSE.txt.");
            Console.WriteLine();
            while(true) {
                Console.Write("> ");
                var input = Console.ReadLine();

                if (input == "exit" || input == "quit") return;

                var csc = new CSharpCodeProvider(new Dictionary<string, string>() {
                  { "CompilerVersion", "v3.5" }
                });
                var parameters = new CompilerParameters(new[] {
                    "mscorlib.dll",
                    "System.Core.dll"
                });
                parameters.GenerateInMemory = true;

                var results = csc.CompileAssemblyFromSource(parameters, $@"
                    using System;
                    class RuntimeCompiled {{
                        public static object Exec() {{
                            return ({input});
                        }}
                    }}
                ");

                var die = false;
                for (int i = 0; i < results.Errors.Count; i++) {
                    var err = results.Errors[i];
                    if (err.IsWarning) {
                        Console.WriteLine($"Warning");
                    } else {
                        die = true;
                        Console.WriteLine($"Compilation error");
                    }
                    Console.WriteLine($"\t{err.ErrorText}");
                }
                if (die) continue;

                var method_info = results.CompiledAssembly.GetType("RuntimeCompiled").GetMethod("Exec");

                object result;
                try {
                    result = method_info.Invoke(null, null);
                } catch(System.Reflection.TargetInvocationException e) {
                    Console.WriteLine($"Runtime error");
                    Console.WriteLine($"\t{e.InnerException.Message}");
                    continue;
                }

                if (result == null) {
                    Console.WriteLine("=> null");
                } else {
                    Console.WriteLine(result);
                    Console.WriteLine($"=> {result.GetType().FullName}");
                }
            }
        }
    }
}
